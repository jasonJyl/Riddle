using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Role2 : MonoBehaviour
{
    private float speedX = 0f;
    private float speedY = 0f;
    private int direction = 1;
    private float speedScale = 5f;

    private float gravity = 0.05f;
    private bool isDead = false;

    private Rigidbody2D _rigidbody2D;
    void Start()
    {
        _rigidbody2D = this.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        
        if (isDead) 
            return;
        
        if (Input.GetKey(KeyCode.LeftArrow)) {
            speedX = -5;
        }
        if (Input.GetKey(KeyCode.RightArrow)) {
            speedX = 5;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            speedY = 0.1f;
        }
        
        _rigidbody2D.velocity = new Vector2(speedX,speedY);
    }

    
    private bool isOnGround = false;
    public void stand()
    {
        this.isOnGround = true;
    }
    
    public void dead()
    {
        this.isDead = true;
    }
    
}
