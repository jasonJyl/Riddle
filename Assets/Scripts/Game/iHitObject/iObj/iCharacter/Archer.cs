using UnityEngine;

namespace TempUseNameSpace
{
    public class Archer:Role
    {
        public GameObject arrow;

        public override void Skill()
        {
            base.Skill();

            var go = Instantiate(arrow);
            go.GetComponent<BaseObject>().position = this.position + new MyPosition(1* this.direction, 0) ;
            go.GetComponent<BaseObject>().direction = this.direction;


        }
    }
}