using UnityEngine;

namespace TempUseNameSpace
{
    public class Fighter:Role
    {
        public GameObject punch;

        public override void Skill()
        {
            base.Skill();

            var go = Instantiate(punch);
            go.GetComponent<BaseObject>().position = this.position + new MyPosition(1* this.direction, 0);
            go.GetComponent<BaseObject>().direction = this.direction;
            
        }
    }
}