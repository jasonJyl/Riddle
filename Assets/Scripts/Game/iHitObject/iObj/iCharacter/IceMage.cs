using UnityEngine;

namespace TempUseNameSpace
{
    public class IceMage:Role
    {
        public GameObject iceball;

        public override void Skill()
        {
            base.Skill();

            var go = Instantiate(iceball);
            go.GetComponent<BaseObject>().position = this.position + new MyPosition(1* this.direction, 0);
            go.GetComponent<BaseObject>().direction = this.direction;
        }
    }
}