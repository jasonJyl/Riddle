
using UnityEngine;

namespace TempUseNameSpace
{
    public class Role:BaseObject
    {
        public bool isDead = false;
        public GameObject ice;
        private Animator _animator;

        protected override void Awake()
        {
            base.Awake();
            this._animator = this.GetComponentInChildren<Animator>();

            this.priority = 2;
        }

        public void Jump(float speed)
        {
            var gravity = this.GetComponent<FuncGravity>();
            if (gravity != null) {
                if (gravity.isOnGround) {
                    gravity.isJump = true;
                    this.speedY = speed;
                }
            }
        }
        
        public void Kill()
        {
            isDead = true;
            Debug.Log("Dead");
            
            _animator.SetTrigger("Die");
            
        }

        public void Freeze()
        {
            var go = Instantiate(ice);
            go.GetComponent<BaseObject>().position = this.position;
            
            GameManager.Instance.RemoveObject(this);
            
        }
        
        virtual public void Skill()
        {
            Debug.Log("Skill");
        }
        
    }
}