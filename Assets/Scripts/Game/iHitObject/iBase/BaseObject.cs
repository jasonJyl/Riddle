﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TempUseNameSpace
{
    public class BaseObject : MonoBehaviour
    {
        public int priority = 0;

        public float speedX {
            set { speed.x = value; }
            get { return speed.x; }
        }
        public float speedY {
            set { speed.y = value; }
            get { return speed.y; }
        }
        public int direction = 1;

        public bool isThrowable = false;
        public Collider2D hitBox;

        public MyPosition speed;
        public MyPosition position;
        public MyPosition hitOffset;
        public MyPosition hitSize;
        
        public MyPosition hitBoxPositionData {
            get {
                return position+hitOffset;
            }
        }
        
        virtual protected void Awake()
        {
            GameManager.Instance.AddObject(this);
            hitBox = this.GetComponent<Collider2D>();
            
            this.speed = new MyPosition();
            
            this.hitOffset = new MyPosition();
            this.hitOffset.x = this.hitBox.offset.x;
            this.hitOffset.y = this.hitBox.offset.y;
            
            this.hitSize = new MyPosition();
            this.hitSize.x = this.hitBox.bounds.size.x;
            this.hitSize.y = this.hitBox.bounds.size.y;

            this.position = new MyPosition();
            this.position.x = this.gameObject.transform.position.x;
            this.position.y = this.gameObject.transform.position.y;
        }

        public void Run()
        {
            var script = this.GetComponents<FunctionBase>();
            foreach (var scr in script) {
                scr.dealBefore();
            }
            
            checkHit();
            
            foreach (var scr in script) {
                scr.dealAfter();
            }
        }

        private void checkHit()
        {
            
            var script = this.GetComponents<FunctionBase>();
            
            var totalFunctionList = new List<FunctionBase>();
            
            var hitList = GameManager.Instance.GetHitObject(this);
            foreach (var hit in hitList) {
                var list = hit.deal();
                totalFunctionList.AddRange(list);

                foreach (var functionBase in script) {
                    if (functionBase != null) {
                        functionBase.positiveDeal(hit);
                    }
                }
            }
            
            
            
            totalFunctionList.Sort((a, b) => { return a.priority.CompareTo(b.priority);});
            foreach (var function in totalFunctionList) {
                function.deal(function.hitInfo);
            }
            
        }


        public bool have<T>()
        {
            var script = this.GetComponent<T>();
            return script != null;
        }

        public void updatePosition()
        {
            MyPosition pos = new MyPosition();
            pos.x = speed.x * direction * Time.deltaTime;
            pos.y = speed.y * Time.deltaTime;
            
            position += pos;
            this.gameObject.transform.position = new Vector3(position.x,position.y,0);
            
        }
        
        public void dispose()
        {
            GameManager.Instance.RemoveObject(this);
        }
    }

}

