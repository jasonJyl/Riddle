using UnityEngine;

namespace TempUseNameSpace
{
    public class FuncGravity:FunctionBase
    {
        private static float gravity = 20f;
        public bool isOnGround { get; set; } = false;
        public bool isJump = false;

        public override void dealBefore()
        {
            this.isOnGround = false;
        }

        public override void dealAfter()
        {
            if (!this.isOnGround) {
                if (baseObject != null) {
                    baseObject.speedY -= gravity * Time.deltaTime;
                }
            }
            else {
                if (baseObject != null) {
                    baseObject.speedY = 0;
                }
            }

            isJump = false;
        }
        
        public void setOnGround(bool flag)
        {
            if (!isJump) {
                isOnGround = true;
            }
            
        }
    }
}