namespace TempUseNameSpace
{
    public class FuncHitFroze:FunctionBase
    {
        public override void positiveDeal(HitInformation info)
        {
            var target = info.hitTarget;
            
            if (target is Role) {
                var role = target as Role;
                role.Freeze();
                GameManager.Instance.RemoveObject(this.baseObject);
            }

        }
    }
}