using System.Collections.Generic;
using UnityEngine;

namespace TempUseNameSpace
{
    public class FuncStick:FunctionBase
    {
        public Role stickTarget;
        public bool isStick = false;
        public override void positiveDeal(HitInformation info)
        {
            if (isStick) {
                stickTarget.position += new MyPosition(this.baseObject.speedX*this.baseObject.direction*Time.deltaTime,0);
                return;
            }
            
            var target = info.hitTarget;
            var direction = info.direction;
            List<eDirection> arr_direction = new List<eDirection>(){eDirection.Left,eDirection.Right};
            if (arr_direction.Contains(direction) && this.baseObject.speedX!=0) {
                if (target is Role) {
                    target.position += new MyPosition(this.baseObject.speedX*this.baseObject.direction*Time.deltaTime,0);
                    stickTarget = target as Role;
                    isStick = true;

                    var gravity = target.GetComponent<FuncGravity>();
                    if (gravity) {
                        Destroy(gravity);
                    }
                    
                    return;
                }
            }
            isStick = false;
        }
    }
}