using System.Collections.Generic;
using UnityEngine;

namespace TempUseNameSpace
{
    public class FuncPushable:FunctionBase
    {
        public override void deal(HitInformation info)
        {
            var target = info.hitFrom;
            var direction = info.direction;
            List<eDirection> arr_direction = new List<eDirection>(){eDirection.Left,eDirection.Right};
            if (arr_direction.Contains(direction) && target.speedX != 0) {
                if ((direction == eDirection.Left && target.direction == 1) ||
                    (direction == eDirection.Right && target.direction == -1)) {
                    var funcNoPushPower = target.GetComponent<FuncNoPushPower>();
                    if (funcNoPushPower != null) {
                        return;
                    }

                    

                    bool ishit = false;
                    var list = GameManager.Instance.GetHitObject(this.baseObject);
                    foreach (var hitInformation in list) {
                        if (hitInformation.direction == direction) {
                            ishit = true;
                            break;
                        }
                    }

                    if (!ishit) {
                        target.speedX /= 2f;
                        this.baseObject.position += new MyPosition(target.speedX * target.direction * Time.deltaTime,0);
                    }
                }
            }
        }

    }
}