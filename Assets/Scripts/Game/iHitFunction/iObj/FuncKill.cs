namespace TempUseNameSpace
{
    public class FuncKill:FunctionBase
    {
        public override void deal(HitInformation info)
        {
            base.deal(info);

            var target = info.hitFrom;
            if (target is Role) {
                (target as Role).Kill();
            }
        }
    }
}