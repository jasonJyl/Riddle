using System.Collections.Generic;

namespace TempUseNameSpace
{
    public class FuncStopAtWall:FunctionBase
    {
        public override void positiveDeal(HitInformation info)
        {
            var target = info.hitTarget;
            var direction = info.direction;
            List<eDirection> arr_direction = new List<eDirection>(){eDirection.Left,eDirection.Right};
            if (arr_direction.Contains(direction)) {
                if (target is Wall) {
                    this.baseObject.speedX = 0;
                    var funcFly = this.baseObject.GetComponent<FuncFly>();
                    if (funcFly != null) {
                        Destroy(funcFly);
                    }

                    var funcStick = this.baseObject.GetComponent<FuncStick>();
                    if (funcStick != null) {
                        if (funcStick.isStick) {
                            DestroyImmediate(this);
                        }
                        else {
                            GameManager.Instance.RemoveObject(this.baseObject);
                        }
                    }
                }
                else if(!(target is Role)){
                    var funcStick = this.baseObject.GetComponent<FuncStick>();
                    if (funcStick != null) {
                        if (funcStick.isStick) {
                            var gravity = funcStick.stickTarget.gameObject.AddComponent<FuncGravity>();
                            gravity.isOnGround = false;
                            GameManager.Instance.RemoveObject(this.baseObject);
                            DestroyImmediate(funcStick);
                        }
                    }
                }
            }
        }
    }
}