using UnityEngine;

namespace TempUseNameSpace
{
    public class FuncPunch:FunctionBase
    {
        public override void deal(HitInformation info)
        {
            var target = info.hitFrom;
            if (target is Role) {
                var role = target as Role;
                role.speedX = 6f;
                role.direction = this.baseObject.direction;
                role.position += new MyPosition(0.1f,0);
                role.Jump(5f);
                var gravity = role.GetComponent<FuncGravity>();
                if (gravity != null) {
                    gravity.isOnGround = false;
                }
                role.Kill();
            }

        }

        public override void dealAfter()
        {
            dispose();
        }
    }
}