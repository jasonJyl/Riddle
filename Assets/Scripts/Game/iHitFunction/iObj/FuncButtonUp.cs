using System.Collections.Generic;
using UnityEngine;

namespace TempUseNameSpace
{
    public class FuncButtonUp:FunctionBase
    {
        public Button btn;
        
        private float count = 0;
        private bool isPushDown = false;
        public override void dealBefore()
        {
            count -= Time.deltaTime;
            if (count <= 0) {
                isPushDown = false;
            }
        }

        public override void deal(HitInformation info)
        {
            var target = info.hitFrom;
            var direction = info.direction;
            List<eDirection> arr_direction = new List<eDirection>(){eDirection.Top};
            if (arr_direction.Contains(direction)) {
                if (target.have<FuncGravity>()) {
                    isPushDown = true;
                    count = 0.1f;
                }
            }
        }

        public override void dealAfter()
        {
            if (!isPushDown && count<=0) {
                buttonUp();
            }
        }

        public void PushDown()
        {
            count = 0.5f;
        }
        
        private void buttonUp()
        {
            btn.doUp();
        }
    }
}