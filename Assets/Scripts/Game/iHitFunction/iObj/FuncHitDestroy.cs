namespace TempUseNameSpace
{
    public class FuncHitDestroy:FunctionBase
    {
        public override void positiveDeal(HitInformation info)
        {
            dispose();
        }
    }
}