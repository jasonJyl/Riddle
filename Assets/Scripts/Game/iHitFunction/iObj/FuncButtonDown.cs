using System.Collections.Generic;

namespace TempUseNameSpace
{
    public class FuncButtonDown:FunctionBase
    {
        public Button btn;
        
        public override void deal(HitInformation info)
        {
            var target = info.hitFrom;
            var direction = info.direction;
            List<eDirection> arr_direction = new List<eDirection>(){eDirection.Top};
            if (arr_direction.Contains(direction)) {
                if(target.have<FuncGravity>())
                buttonDown();
            }
        }

        private void buttonDown()
        {
            btn.doDown();
        }
    }
}