using System.Collections.Generic;

namespace TempUseNameSpace
{
    public class FuncStopFromSideMove:FunctionBase
    {
        public override void deal(HitInformation info)
        {
            var target = info.hitFrom;
            var direction = info.direction;
            List<eDirection> arr_direction = new List<eDirection>(){eDirection.Left,eDirection.Right};
            if (arr_direction.Contains(direction) && target.speedY == 0) {
                var gravity = target.GetComponent<FuncGravity>();
                if (gravity != null && gravity.isOnGround) {
                    info.UpdatePosition(true);
                }
            }
        }
    }
}