namespace TempUseNameSpace
{
    public class FuncStand:FunctionBase
    {

        public override void deal(HitInformation info)
        {
            var target = info.hitFrom;
            var direction = info.direction;
            if (direction == eDirection.Top && target.speedY<=0) {
                var gravity = target.GetComponent<FuncGravity>();
                if (gravity != null) {
                    gravity.setOnGround(true);
                    info.UpdatePosition(true);
                }
            }
        }
    }
}