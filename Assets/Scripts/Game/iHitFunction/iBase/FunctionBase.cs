using UnityEngine;

namespace TempUseNameSpace
{
    public abstract class FunctionBase:MonoBehaviour
    {
        public int priority = 0;
        public HitInformation hitInfo;
        protected BaseObject baseObject {
            get { return this.GetComponent<BaseObject>(); }
        }

        public virtual void dealBefore()
        {
            
        }

        public virtual void deal(HitInformation info)
        {
            
        }

        public virtual void dealAfter()
        {
            
        }

        public virtual void positiveDeal(HitInformation info)
        {
            
        }

        public void dispose()
        {
            GameManager.Instance.RemoveObject(this.baseObject);
        }
    }
}