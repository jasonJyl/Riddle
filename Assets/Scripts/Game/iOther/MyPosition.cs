using System;
using UnityEngine;

namespace TempUseNameSpace
{
    [Serializable]
    public class MyPosition
    {
        public AccurateFloat x;
        public AccurateFloat y;

        public MyPosition(){}

        public MyPosition(float _x, float _y)
        {
            this.x = _x;
            this.y = _y;
        }

        public new string ToString()
        {
            return this.x + "," + this.y;
        }
        
        public static MyPosition operator +(MyPosition a,MyPosition b)
        {
            MyPosition c = new MyPosition();
            c.x = a.x + b.x;
            c.y = a.y + b.y;
            return c;
        }

        public static MyPosition operator -(MyPosition a, MyPosition b)
        {
            MyPosition c = new MyPosition();
            c.x = a.x - b.x;
            c.y = a.y - b.y;
            return c;
        }
    }
}