using UnityEngine;

namespace TempUseNameSpace
{
    public class AccurateTransform
    {
        public MyPosition myPosition;
        public Vector3 position {
            set {
                myPosition.x = value.x;
                myPosition.y = value.y;
            }
            get {
                return new Vector3(myPosition.x,myPosition.y,0);
            }
        }
    }
}