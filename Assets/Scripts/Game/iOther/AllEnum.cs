namespace TempUseNameSpace
{
    public static class AllEnum
    {
        public static eDirection GetOpposite(eDirection e)
        {
            switch (e) {
                case eDirection.Bottom:
                    return eDirection.Top;
                case eDirection.Top:
                    return eDirection.Bottom;
                case eDirection.Left:
                    return eDirection.Right;
                case eDirection.Right:
                    return eDirection.Left;
                default:
                    return eDirection.UnKnow;
            }
        }
    }

    public enum eDirection
    {
        UnKnow,
        Left,
        Right,
        Top,
        Bottom,
        Corner,
    }
}