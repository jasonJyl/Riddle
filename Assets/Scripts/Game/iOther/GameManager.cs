﻿using System;
using System.Collections;
using System.Collections.Generic;
using TempUseNameSpace;
using UnityEditor;
using UnityEngine;

namespace TempUseNameSpace
{
    
    public class GameManager : MonoSingleton<GameManager>
    {
        public List<BaseObject> objectList;
        public List<Role> roleList;
        public AccurateFloat a;
        private GameManager()
        {
            objectList = new List<BaseObject>();
            roleList = new List<Role>();

            a = 1;
            a += 1;
            a += 0.1f;
            Debug.Log(a.ToString());

        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.X)) {
                changeCharacter();
            }
        }

        private void changeCharacter()
        {
            bool isNext = false;
            foreach (var role in roleList) {
                if (isNext) {
                    if (role.isDead) {
                        continue;
                    }
                    role.gameObject.AddComponent<RoleController>();
                    isNext = false;
                    break;
                }
                
                var roleController = role.GetComponent<RoleController>();
                if (roleController != null) {
                    Destroy(roleController);
                    isNext = true;
                }
            }

            if (isNext) {
                foreach (var role in roleList) {
                    if (role.isDead) {
                        continue;
                    }
                    role.gameObject.AddComponent<RoleController>();
                    isNext = false;
                    break;
                }
            }

        }

        public void FixedUpdate()
        {
            objDo((obj) => {
                if (!obj.gameObject.activeSelf)
                    return;
                obj.Run();
            });
            
            objDo((obj) => {
                if (!obj.gameObject.activeSelf)
                    return;
                
                obj.updatePosition();
                obj.gameObject.transform.localScale = new Vector3(obj.direction, 1, 1);
                if (obj is Role) {
                    var role = obj as Role;
                    if (role.isDead && role.have<FuncGravity>() && !role.GetComponent<FuncGravity>().isOnGround) {
                        
                    }
                    else {
                        role.speedX = 0;
                    }
                }
                else {
                    obj.speedX = 0;
                }
            });
            

            objDo((obj) => {
                if(!obj.gameObject.activeSelf)
                    return;
                
                restrictPosition(obj);
            });
            
        }


        public void AddObject(BaseObject baseObject)
        {
            this.objectList.Add(baseObject);
            if (baseObject is Role) {
                roleList.Add(baseObject as Role);
                if (roleList.Count == 1) {
                    baseObject.gameObject.AddComponent<RoleController>();
                }
            }
        }

        public void RemoveObject(BaseObject baseObject)
        {
            this.objectList.Remove(baseObject);
            Destroy(baseObject.gameObject);
        }
        
        public List<HitInformation> GetHitObject(BaseObject hitObject)
        {
            List<HitInformation> hitList = new List<HitInformation>();
            
            var pushable = hitObject.GetComponent<FuncPushable>();
            
            foreach (var o in objectList) {
                if(!o.gameObject.activeSelf)
                    continue;
                if (o!=hitObject && HitInformation.IsHitEachOther(hitObject,o) && (hitObject is Role|| hitObject is SkillObject || pushable!=null)) {
                    var x_gap = hitObject.hitBoxPositionData.x - o.hitBoxPositionData.x;
                    var y_gap = hitObject.hitBoxPositionData.y - o.hitBoxPositionData.y;
                    var hitInfo = new HitInformation(hitObject,o);
                    hitInfo.UpdateDirection(x_gap,y_gap);
                    hitList.Add(hitInfo);
                }                
            }

            return hitList;
        }

        private void restrictPosition(BaseObject obj)
        {
            var pushable = obj.GetComponent<FuncPushable>();
            if (pushable != null) {
                {
                    var list = GetHitObject(obj);
                    foreach (var hitInformation in list) {
                        if (hitInformation.direction == eDirection.Left || hitInformation.direction == eDirection.Right)
                            hitInformation.UpdatePosition();
                    }
                }
                {
                    var list = GetHitObject(obj);

                    foreach (var hitInformation in list) {
                        if (hitInformation.direction == eDirection.Top || hitInformation.direction == eDirection.Bottom)
                            hitInformation.UpdatePosition();
                    }
                }
            }
        }


        private void objDo(Action<BaseObject> act)
        {
            List<BaseObject> obj = new List<BaseObject>();
            obj = new List<BaseObject>(this.objectList);
            obj.Sort((a, b) => {return b.priority.CompareTo(a.priority);});
            foreach (var baseObject in obj) {
                act(baseObject);
            }
        }
    }
}

