﻿using System;
using UnityEngine;

namespace TempUseNameSpace
{
	[Serializable]
	public struct AccurateFloat : IEquatable<AccurateFloat>, IFormattable
	{
		public const float scaleRound = 100000f;
		public const int scale = 1000000;

        [SerializeField]
		private int value;


//		private AccurateInt(int _value)
//		{
//			this.value = ChangeFrom(_value);
//		}

		private AccurateFloat(float _value)
		{
			this.value = ChangeFrom(_value);
		}

		private static int ChangeFrom(int _value)
		{
			return _value * scale;
		}
		
		private static int ChangeFrom(float _value)
		{
			var roundf = Mathf.Round(_value * scaleRound);
			int a = (int)(roundf * 10);
			return a;
		}

		public int ChangeToInt()
		{
			return value / scale;
		}

		public float ChangeToFloat()
		{
			return (float) value / scale;
		}
		
		#region operators, overrides, interface implementations
		//! @cond
//		public static implicit operator AccurateInt(int value)
//		{
//			AccurateInt accurate = new AccurateInt(value);
//			return accurate;
//		}
//
//		public static implicit operator int(AccurateInt value)
//		{
//			return value.ChangeToInt();
//		}
		
		public static implicit operator AccurateFloat(float value)
		{
			AccurateFloat accurate = new AccurateFloat(value);
			return accurate;
		}

		public static implicit operator float(AccurateFloat value)
		{
			return value.ChangeToFloat();
		}
		
		public static AccurateFloat operator +(AccurateFloat a,AccurateFloat b)
		{
			AccurateFloat c = new AccurateFloat(0);
			c.value = a.value + b.value;
			return c;
		}

		public static AccurateFloat operator -(AccurateFloat a,AccurateFloat b)
		{
			AccurateFloat c = new AccurateFloat(0);
			c.value = a.value - b.value;
			return c;
		}
		
		
//		public static AccurateFloat operator ++(AccurateFloat input)
//		{
//			input += scale;
//			return input;
//		}
//
//		public static AccurateFloat operator --(AccurateFloat input)
//		{
//			input -= scale;
//			return input;
//		}

		public override bool Equals(object obj)
		{
			if (!(obj is AccurateFloat))
				return false;
			return Equals((AccurateFloat)obj);
		}

		public bool Equals(AccurateFloat obj)
		{
			return value == obj.value;
		}

		public override int GetHashCode()
		{
			return value.GetHashCode();
		}

		public override string ToString()
		{
			return this.ChangeToFloat().ToString();
		}

		
		public string ToString(string format)
		{
			return this.ChangeToFloat().ToString(format);
		}

		public string ToString(IFormatProvider provider)
		{
			return this.ChangeToFloat().ToString(provider);
		}

		public string ToString(string format, IFormatProvider provider)
		{
			return this.ChangeToFloat().ToString(format, provider);
		}

		//! @endcond
#endregion

	}
}