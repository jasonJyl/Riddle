using UnityEngine;

namespace TempUseNameSpace
{
    public class HitInformation
    {
        public BaseObject hitFrom;
        public BaseObject hitTarget;
        public eDirection direction = eDirection.UnKnow;

        private MyPosition standardDistance {
            get {
                MyPosition pos =  hitFrom.hitSize+hitTarget.hitSize;
                pos.x /= 2f;
                pos.y /= 2f;
                return pos;
            }
        }
        
        public HitInformation(BaseObject _from,BaseObject _target)
        {
            this.hitFrom = _from;
            this.hitTarget = _target;
        }

        private float getAngle()
        {
            var hitsize = hitFrom.hitSize + hitTarget.hitSize;
            var radians = Mathf.Atan2(hitsize.y,hitsize.x);
            var angle = radians / Mathf.PI * 180;
            return angle;
        }
        
        public void UpdateDirection(float x, float y)
        {
            var tan = y / x;
            var radians = Mathf.Atan2(y,x);
            var angle = radians / Mathf.PI * 180;

            var directionAngle = getAngle();
//            directionAngle += 1;
            
            if (angle > -directionAngle && angle < directionAngle) {
                direction = eDirection.Right;
            }else if (angle > directionAngle && angle < 180-directionAngle) {
                direction = eDirection.Top;
            }else if (angle < -directionAngle && angle > -(180-directionAngle)) {
                direction = eDirection.Bottom;
            }else if (angle == -directionAngle || angle == directionAngle || angle == 180 - directionAngle ||
                      angle == -(180 - directionAngle)) {
                direction = eDirection.Corner;
            }
            else {
                direction = eDirection.Left;
            }
        }

        public void UpdatePosition(bool isForce = false)
        {
            if (hitTarget.isThrowable&&!isForce)
                return;

//            if (hitFrom is Role && hitTarget is Role)
//                return;

            if (hitFrom.have<FuncNoPushPower>() || hitTarget.have<FuncNoPushPower>())
                return;

            var posGap = hitFrom.hitBoxPositionData - hitTarget.hitBoxPositionData;
            var distance = standardDistance;

            var gapX = Mathf.Abs(posGap.x) - distance.x;
            var gapY = Mathf.Abs(posGap.y) - distance.y;
            switch (direction) {
                case eDirection.Left:
                    if (gapX < 0) {
                        hitFrom.position += new MyPosition(gapX,0);
                        Debug.Log(eDirection.Left);
                    }
                    break;
                case eDirection.Right:
                    if (gapX < 0) {
                        hitFrom.position -= new MyPosition(gapX,0);
                        Debug.Log(eDirection.Right);
                    }
                    break;
                case eDirection.Top:
                    if (gapY < 0) {
                        hitFrom.position -= new MyPosition(0,gapY);
                        Debug.Log(eDirection.Top);
                    }
                    break;
                case eDirection.Bottom:
                    if (gapY < 0) {
                        hitFrom.position += new MyPosition(0,gapY);
                        Debug.Log(eDirection.Bottom);
                        hitFrom.speedY = 0;
                    }
                    break;
            }
        }
        
        public FunctionBase[] deal()
        {
            var list =  hitTarget.GetComponents<FunctionBase>();
            foreach (var function in list) {
                function.hitInfo = this;
            }

            return list;
        }

        public static bool IsHitEachOther(BaseObject a, BaseObject b)
        {
            var posGap = a.hitBoxPositionData - b.hitBoxPositionData;
            var distance = a.hitSize + b.hitSize;
            distance.x /= 2f;
            distance.y /= 2f;
            
            var gapX = Mathf.Abs(posGap.x) - distance.x;
            var gapY = Mathf.Abs(posGap.y) - distance.y;

//            if (gapY == 0 && a is Role) {
//                Debug.Log(gapX+","+gapY);
//            }
            
            if (gapX <= 0 && gapY <= 0) {
                return true;
            }

            return false;
        }
    }
}