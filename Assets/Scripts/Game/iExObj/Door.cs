using UnityEngine;

namespace TempUseNameSpace
{
    public class Door:MonoBehaviour
    {
        public GameObject open;
        public GameObject close;

        public void doOpen()
        {
            close.gameObject.SetActive(false);
        }

        public void doClose()
        {
            close.gameObject.SetActive(true);
        }
        
    }
}