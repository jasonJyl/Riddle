using System;
using UnityEngine;

namespace TempUseNameSpace
{
    public class Button:MonoBehaviour
    {
        [SerializeField]
        private GameObject up;
        [SerializeField]
        private GameObject down;

        public Door door;


        private void Awake()
        {
            var scriptUP = up.GetComponent<FuncButtonDown>();
            scriptUP.btn = this;

            var scriptDown = down.GetComponent<FuncButtonUp>();
            scriptDown.btn = this;
        }

        public void doUp()
        {
            up.gameObject.SetActive(true);
            
            door.doClose();
        }

        public void doDown()
        {
            up.gameObject.SetActive(false);
            
            var scriptBtnUp = down.GetComponent<FuncButtonUp>();
            scriptBtnUp.PushDown();
            
            door.doOpen();
        }
    }
}