using System;
using UnityEngine;

namespace TempUseNameSpace
{
    public class RoleController:MonoBehaviour
    {
        public void Update()
        {
            var role = this.GetComponent<Role>();
            if (role == null)
                return;
            if (role.isDead) {
                return;
            }
            
            if (Input.GetKey(KeyCode.LeftArrow)) {
                role.speedX = 4;
                if (role.direction != -1) {
                    role.direction = -1;
                    role.speedX = 0;
                }
            }
            if (Input.GetKey(KeyCode.RightArrow)) {
                role.speedX = 4;
                if (role.direction != 1) {
                    role.direction = 1;
                    role.speedX = 0;
                }
                
            }
            if (Input.GetKeyDown(KeyCode.UpArrow)) {
                role.Jump(10f);
            }

            if (Input.GetKeyDown(KeyCode.Z)) {
                role.Skill();
            }
        }
    }
}