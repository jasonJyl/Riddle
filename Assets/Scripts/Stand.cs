﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stand : MonoBehaviour
{
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        var role  = other.gameObject.GetComponent<Role>();
        role.stand();
        
        Debug.Log("Stand");
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        var role  = other.gameObject.GetComponent<Role>();
        role.stand();
        
        Debug.Log("Stand");
    }
}
