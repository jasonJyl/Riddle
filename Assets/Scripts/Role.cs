﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Role : MonoBehaviour
{
    private float speedX = 0f;
    private float speedY = 0f;
    private int direction = 1;
    private float speedScale = 5f;

    private float gravity = 0.05f;
    private bool isDead = false;

    private Rigidbody2D _rigidbody2D;
    void Start()
    {
        _rigidbody2D = this.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        updatePosition();
        
        if (isDead) 
            return;
        
        if (Input.GetKey(KeyCode.LeftArrow)) {
            speedX = 5;
            direction = -1;
        }
        if (Input.GetKey(KeyCode.RightArrow)) {
            speedX = 5;
            direction = 1;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            if (isOnGround) {
                speedY = 0.03f;
                isOnGround = false;
            }
        }
        
        
        
    }

    private void updatePosition()
    {
        if (isOnGround) {
            this._rigidbody2D.velocity = new Vector2(speedX*this.direction,0);
        }
        else {
            this._rigidbody2D.velocity = new Vector2(speedX*this.direction,speedY);
        }
        
        
        this.transform.localScale = new Vector3(direction,1,1);
        this.speedX -= Time.deltaTime*speedScale;
        if (this.speedX <= 0) {
            this.speedX = 0;
        }

        if (!isOnGround) {
            this.speedY -= Time.deltaTime * gravity;
        }

    }

    private bool isOnGround = false;
    private void OnCollisionEnter2D(Collision2D other)
    {
//        Debug.Log("Role Collision");
//        isOnGround = true;
//
//        foreach (ContactPoint2D hitPos in other.contacts) {
//            Debug.Log(hitPos.point);
//        }
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
//        foreach (ContactPoint2D hitPos in other.contacts) {
//            Debug.Log(hitPos.point);
//        }
    }


    public void stand()
    {
        this.isOnGround = true;
    }
    
    public void dead()
    {
        this.isDead = true;
    }
    
}
